<?php
class ControllerShippingMileage extends Controller { 
	private $error = array();
	
	public function index() {  
		$this->load->language('shipping/mileage');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				 
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('mileage', $this->request->post);	

			$this->session->data['success'] = $this->language->get('text_success');
									
			$this->redirect($this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'));
		}
		
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_none'] = $this->language->get('text_none');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		
		$this->data['text_all_zones'] = $this->language->get('text_all_zones');
		$this->data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		
		$this->data['entry_mileage_city'] = $this->language->get('entry_mileage_city');
		$this->data['entry_mileage_store'] = $this->language->get('entry_mileage_store');
		
		$this->data['entry_mileage_city_rate'] = $this->language->get('entry_mileage_city_rate');
		$this->data['entry_mileage_oblast_rate'] = $this->language->get('entry_mileage_oblast_rate');
		$this->data['entry_max_distance'] = $this->language->get('entry_max_distance');
		$this->data['entry_hide_map'] = $this->language->get('entry_hide_map');
		$this->data['entry_tax_class'] = $this->language->get('entry_tax_class');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_shipping'),
			'href'      => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('shipping/mileage', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('shipping/mileage', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'); 

		$this->load->model('localisation/geo_zone');

		if (isset($this->request->post['mileage_store'])) {
			$this->data['mileage_store'] = $this->request->post['mileage_store'];
		} else {
			$this->data['mileage_store'] = $this->config->get('mileage_store');
		}

		if (isset($this->request->post['mileage_city'])) {
			$this->data['mileage_city'] = $this->request->post['mileage_city'];
		} else {
			$this->data['mileage_city'] = $this->config->get('mileage_city');
		}
		
		if (isset($this->request->post['mileage_city_rate'])) {
			$this->data['mileage_city_rate'] = $this->request->post['mileage_city_rate'];
		} else {
			$this->data['mileage_city_rate'] = $this->config->get('mileage_city_rate');
		}
		
		if (isset($this->request->post['mileage_oblast_rate'])) {
			$this->data['mileage_oblast_rate'] = $this->request->post['mileage_oblast_rate'];
		} else {
			$this->data['mileage_oblast_rate'] = $this->config->get('mileage_oblast_rate');
		}
		
		if (isset($this->request->post['mileage_max_distance'])) {
			$this->data['mileage_max_distance'] = $this->request->post['mileage_max_distance'];
		} else {
			$this->data['mileage_max_distance'] = $this->config->get('mileage_max_distance');
		}
		
		if (isset($this->request->post['mileage_hide_map'])) {
			$this->data['mileage_hide_map'] = $this->request->post['mileage_hide_map'];
		} else {
			$this->data['mileage_hide_map'] = $this->config->get('mileage_hide_map');
		}
		
		if (isset($this->request->post['mileage_status'])) {
			$this->data['mileage_status'] = $this->request->post['mileage_status'];
		} else {
			$this->data['mileage_status'] = $this->config->get('mileage_status');
		}		
		
		$this->load->model('localisation/geo_zone');
		
		$this->data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();
		
		if (isset($this->request->post['mileage_geo_zone_id'])) {
			$this->data['mileage_geo_zone_id'] = $this->request->post['mileage_geo_zone_id'];
		} else {
			$this->data['mileage_geo_zone_id'] = $this->config->get('mileage_geo_zone_id');
		}
		
		if (isset($this->request->post['mileage_tax_class_id'])) {
			$this->data['mileage_tax_class_id'] = $this->request->post['mileage_tax_class_id'];
		} else {
			$this->data['mileage_tax_class_id'] = $this->config->get('mileage_tax_class_id');
		}
		
		if (isset($this->request->post['mileage_status'])) {
			$this->data['mileage_status'] = $this->request->post['mileage_status'];
		} else {
			$this->data['mileage_status'] = $this->config->get('mileage_status');
		}
		
		if (isset($this->request->post['mileage_sort_order'])) {
			$this->data['mileage_sort_order'] = $this->request->post['mileage_sort_order'];
		} else {
			$this->data['mileage_sort_order'] = $this->config->get('mileage_sort_order');
		}	
		
		$this->load->model('localisation/tax_class');
				
		$this->data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();

		$this->template = 'shipping/mileage.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
		
	private function validate() {
		if (!$this->user->hasPermission('modify', 'shipping/mileage')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>
