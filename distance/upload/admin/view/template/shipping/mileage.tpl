<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <div id="tab-general" class="vtabs-content">
          <table class="form">
            <tr>
              <td><?php echo $entry_tax_class; ?></td>
              <td><select name="mileage_tax_class_id">
                  <option value="0"><?php echo $text_none; ?></option>
                  <?php foreach ($tax_classes as $tax_class) { ?>
                  <?php if ($tax_class['tax_class_id'] == $mileage_tax_class_id) { ?>
                  <option value="<?php echo $tax_class['tax_class_id']; ?>" selected="selected"><?php echo $tax_class['title']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select></td>
            </tr>

			<tr>
			  <td><?php echo $entry_geo_zone; ?></td>
			  <td><select name="mileage_geo_zone_id">
				<option value="0"><?php echo $text_all_zones; ?></option>
				<?php foreach ($geo_zones as $geo_zone) { ?>
				<?php if ($geo_zone['geo_zone_id'] == $mileage_geo_zone_id) { ?>
				<option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected"><?php echo $geo_zone['name']; ?></option>
				<?php } else { ?>
				<option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
				<?php } ?>
				<?php } ?>
			  </select></td>
			</tr>
            
            <tr>
              <td><?php echo $entry_mileage_city; ?></td>
              <td><input type="text" name="mileage_city" value="<?php echo ${'mileage_city'}; ?>" size="40" /></td>
            </tr>

            <tr>
              <td><?php echo $entry_mileage_store; ?></td>
              <td>
				  <input type="text" name="mileage_store" value="<?php echo ${'mileage_store'}; ?>" size="40" />
              </td>
            </tr>
            
            <tr>
              <td><?php echo $entry_mileage_city_rate; ?></td>
              <td><textarea name="mileage_city_rate" cols="40" rows="5"><?php echo ${'mileage_city_rate'}; ?></textarea></td>
            </tr>
            <tr>
              <td><?php echo $entry_mileage_oblast_rate; ?></td>
              <td><textarea name="mileage_oblast_rate" cols="40" rows="5"><?php echo ${'mileage_oblast_rate'}; ?></textarea></td>
            </tr>
            <tr>
              <td><?php echo $entry_max_distance; ?></td>
              <td><input type="text" name="mileage_max_distance" value="<?php echo $mileage_max_distance; ?>" size="4" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_hide_map; ?></td>
              <td><input type="checkbox" name="mileage_hide_map" value="1" <?php echo ($mileage_hide_map == 1 ? ' checked="checked"' : ''); ?> />
			  </td>
            </tr>
            
            <tr>
              <td><?php echo $entry_sort_order; ?></td>
              <td><input type="text" name="mileage_sort_order" value="<?php echo $mileage_sort_order; ?>" size="1" /></td>
            </tr>
            
            <tr>
              <td><?php echo $entry_status; ?></td>
              <td><select name="mileage_status">
                  <?php if (${'mileage_status'}) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
            </tr>
            
          </table>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
$('.vtabs a').tabs(); 
//--></script> 
<?php echo $footer; ?> 
