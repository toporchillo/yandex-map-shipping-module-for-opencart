"Доставка по километражу" модификация OpenCart
автор Александр Топорков (toporchillo@gmail.com)

Поддерживаемые версии: 1.5.x

Модуль рассчитывает стоимость доставки вида:
По Городу: XX руб. за каждый километр.
За город: YY руб. за каждый километр.

Рассчет расстояния ведется от адреса склада до адреса доставки покупателя по Яндекс.Карте.

Установка:

1. Скопируйте файлы модуля (из папки upload) на сайт

2. В файл catalog/view/theme/default/template/common/header.tpl
внутрь тэга <head></head> добавьте строку:

<script src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>
