<?php
// Heading
$_['heading_title']		  = 'Доставка по областям на карте';

// Text
$_['text_extension']   = 'Доставка';
$_['text_success']     = 'Настройки модуля обновлены!';
$_['text_edit']        = 'Редактирование модуля';
$_['text_delete_area'] = 'Удалить район';

// Entry
$_['entry_tax_class']  = 'Налоговый класс:';
$_['entry_geo_zone']   = 'Географическая зона:';
$_['entry_status']     = 'Статус:';
$_['entry_sort_order'] = 'Порядок сортировки:';
$_['entry_show_map'] = 'Показывать карту:';

$_['entry_add_area'] = 'Добавить';
$_['entry_on'] = 'Включено:';
$_['entry_area_name'] = 'Название района:';
$_['entry_price'] = 'Цена доставки:';
$_['entry_points'] = 'Координаты:';

// Error
$_['error_permission']	= 'У Вас нет прав для управления этим модулем!';
