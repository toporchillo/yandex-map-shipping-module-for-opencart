<?php
/**
* Доставка по зонам на карте для OpenCart (ocStore) 2.3.x
*
* @author Alexander Toporkov <toporchillo@gmail.com>
* @copyright (C) 2018- Alexander Toporkov
* @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
*/
class ControllerExtensionShippingMaparea extends Controller {
	private $error = '';

	public $CONFIG = array(
		'status' => 1,
		'tax_class_id' => 0,
		'geo_zone_id' => 0,
		'sort_order' => 100,
		'show_map' => 1,
		'apikey' => '5b0a29c0-c807-4dee-9499-4c44c1d4a3aa',
		'map' => array()
	);
	
	private function cmpBySort($a, $b) {
		if ($a['sort_order'] == $b['sort_order']) {
			return 0;
		}
		return ($a['sort_order'] < $b['sort_order']) ? -1 : 1;
	}	
	
	public function index() {
		$this->load->language('extension/shipping/maparea');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->request->post['maparea_status'] = $this->request->post['maparea_status'];
			
			if (isset($this->request->post['maparea_map']) && is_array($this->request->post['maparea_map']['sort_order'])) {
				$tmp_arr = array();
				foreach($this->request->post['maparea_map']['sort_order'] as $key=>$order) {
					$tmp_arr[] = array('num'=>$key, 'sort_order'=>intval($order));
				}
				uasort($tmp_arr, array($this, 'cmpBySort'));
				$arr = array();
				foreach($this->request->post['maparea_map'] as $key=>$values) {
					$arr[$key] = array();
					foreach($tmp_arr as $item) {
						$arr[$key][] = $values[$item['num']];
					}
				}
				$this->request->post['maparea_map'] = $arr;
			}
			
			$this->model_setting_setting->editSetting('maparea', $this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');
			
			$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=shipping', true));
		}
		$this->setConfig();

		$this->load->model('localisation/tax_class');
		$data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();
		$this->load->model('localisation/geo_zone');
		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['entry_tax_class'] = $this->language->get('entry_tax_class');
		$data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_show_map'] = $this->language->get('entry_show_map');
		
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_all_zones'] = $this->language->get('text_all_zones');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_delete_area'] = $this->language->get('text_delete_area');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		
		$data['entry_add_area'] = $this->language->get('entry_add_area');
		$data['entry_on'] = $this->language->get('entry_on');
		$data['entry_area_name'] = $this->language->get('entry_area_name');
		$data['entry_price'] = $this->language->get('entry_price');
		$data['entry_points'] = $this->language->get('entry_points');
    
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		

 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=shipping', true)
		);
    
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/shipping/maparea', 'token=' . $this->session->data['token'], true)
		);
		
		$data['action'] = $this->url->link('extension/shipping/maparea', 'token=' . $this->session->data['token'], true);

		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=shipping', true);
    
		$data['token'] = $this->session->data['token'];

		foreach($this->CONFIG as $key=>$conf) {
			if (isset($this->request->post['maparea_'.$key])) {
				$data['maparea_'.$key] = $this->request->post['maparea_'.$key];
			} else {
				$data['maparea_'.$key] = $conf;
			}
		}

		$this->load->model('localisation/language');
		
		$data['languages'] = $this->model_localisation_language->getLanguages();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/shipping/maparea', $data));
	}

	private function setConfig() {
		if (!$this->config->get('maparea_set')) {
			return;
		}
	
		foreach($this->CONFIG as $key=>$conf) {
			$this->CONFIG[$key] = $this->config->get('maparea_'.$key);
		}
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'extension/shipping/maparea')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		/*
		else if (!isset($this->request->post['maparea_price'])) {
			$this->error['warning'] = 'Укажите один или несколько статусов заказа, которые не надо синхронизировать со службой доставки.';
		}
		*/
		
		return !$this->error;	
	}
	
	public function uninstall() {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "setting` WHERE `code`='maparea'");
	}
}
