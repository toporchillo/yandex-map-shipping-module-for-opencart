<?php
class ModelExtensionShippingMaparea extends Model {
    public $LOG_LEVEL = 4;
	
	function getQuote($address) {
		if (!$this->config->get('maparea_status')) {
			return array();
		}
		
		$this->load->language('extension/shipping/maparea');

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('maparea_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");
		if (!$this->config->get('maparea_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}

		$method_data = array();

		if ($status) {
			$toaddr = array();
			$toaddr[] = $address['country'];
			$toaddr[] = $address['zone'];
			$toaddr[] = $address['city'];
			$toaddr[] = $address['address_1'];
			$toaddr[] = $address['address_2'];
			$toaddr = array_filter($toaddr, 'strlen');
			$toaddr = implode(', ', $toaddr);
			$toaddr = htmlspecialchars_decode($toaddr);
		
			$latlng = $this->geoCodeAddress($toaddr);
			if (!$latlng) {
				//return $method_data;
			}
			
			$cost = -1;
			$total = $this->cart->getTotal();
			$areas = $this->config->get('maparea_map');
			foreach($areas['points'] as $i=>$area) {
				if ($areas['on'][$i] && ($area == '' || ($latlng && $this->isInArea($area, $latlng)))) {
					$method_title = $areas['name'][$i];
					$cost = $this->getDeliveryPrice($areas['price'][$i], $total);
					break;
				}
			}
			
			$quote_data = array();
			
			if ($cost >= 0) {
				$quote_data['maparea'] = array(
					'code'         => 'maparea.maparea',
					'title'        => $method_title,
					'cost'         => $cost,
					'tax_class_id' => $this->config->get('maparea_tax_class_id'),
					'text'         => $this->currency->format($this->tax->calculate($cost, $this->config->get('maparea_tax_class_id'), $this->config->get('config_tax')), $this->session->data['currency'])
				);

				$method_data = array(
					'code'       => 'maparea',
					'title'      => $this->language->get('text_title') . ($latlng ? $this->getMap($latlng, $toaddr) : ''),
					'quote'      => $quote_data,
					'sort_order' => $this->config->get('maparea_sort_order'),
					'error'      => false
				);
			}
		}
		return $method_data;
	}
	
	private function geoCodeAddress($address) {
		$latlng = $this->cache->get('geocode.'.md5($address));
		if (!$latlng) {
			$url = 'https://geocode-maps.yandex.ru/1.x/?geocode='.urlencode($address).'&apikey='.$this->config->get('maparea_apikey').'&format=json';
			$ch = curl_init ($url);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1) ;
			$result = curl_exec($ch) ;
			//curl_close($ch);

			if(curl_errno($ch)){
				$info = curl_getinfo($ch);
				$this->log($address.' : CURL Error: '.curl_error($ch).'. Took: ' . $info['total_time'] . 'sec. URL: ' . $info['url'], 1);
				return false;
			} 
			else {
				try {
					$info = json_decode($result, true);
				}
				catch(Exception $e) {
					$this->log($address.' : GeoCoder response is not a valid JSON: '.$e->getMessage(), 1);
					return false;
				}
			}
			
			if (!isset($info['response']) || !isset($info['response']['GeoObjectCollection']) || !isset($info['response']['GeoObjectCollection']['featureMember'])
				 || !isset($info['response']['GeoObjectCollection']['featureMember'][0]) || !isset($info['response']['GeoObjectCollection']['featureMember'][0]['GeoObject'])
				 || !isset($info['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point'])
				 || !isset($info['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos'])) {
				$this->log($address.' : Unexpected GeoCoder response: '.$result, 2);
				return false;
			}
			$latlng = $info['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos'];
			$this->cache->set('geocode.'.md5($address), $latlng);
		}
		return explode(' ', $latlng);
	}

	private function getMap($latlng, $address) {
		if (!$this->config->get('maparea_show_map') || strpos($_SERVER['HTTP_REFERER'], 'admin') !== false) {
			return '';
		}
		return <<<EOD
<div id="areamap" style="width: 100%; height: 350px; display: none;"> </div>
<script type="text/javascript">
$(document).ready(function() {
// Как только будет загружен API и готов DOM, выполняем инициализацию
if (ymaps === undefined) {	
	return;
}
$('#areamap').show();

ymaps.ready(initMap);

function initMap() {
	var myMap = new ymaps.Map("areamap", {
		center:[ $latlng[1], $latlng[0] ],
		zoom:14
	});

	// Задает опции метки и отображает метку на карте.
	var placemark = new ymaps.Placemark([ $latlng[1], $latlng[0] ], {
		draggable: 0
	});
	placemark.name = "$address";
	//placemark.description = "Описание";
	myMap.geoObjects.add(placemark);
}
})
</script>
EOD;
	}

	private function isInArea($area, $point) {
		$j=0;
		$xp = array();
		$yp = array();
		$area = explode(',', $area);
		while ($j<count($area)) {
			$xp[] = floatval($area[$j])*1000;
			$yp[] = floatval($area[$j+1])*1000;
			$j+= 2;
		}
		return $this->pinpoly($xp, $yp, array($point[1]*1000, $point[0]*1000));
    }
	
	private function pinpoly($xp, $yp, $latlng) {
		$c = 0;
		$j = count($xp) - 1;
		for ($i = 0; $i < count($xp); $i++) {
			if (((($yp[$i]<=$latlng[1]) && ($latlng[1]<$yp[$j])) || (($yp[$j]<=$latlng[1]) && ($latlng[1]<$yp[$i]))) &&
				($latlng[0] > ($xp[$j] - $xp[$i]) * ($latlng[1] - $yp[$i]) / ($yp[$j] - $yp[$i]) + $xp[$i])) {
				$c = !$c;
			}
			$j = $i;
		}
		return $c;
	}

	private function getDeliveryPrice($price, $total=0) {
		if (strpos($price, ':') === false) {
			return $price;
		}
		$vars = explode('|', $price);
		$ret = false;
		foreach ($vars as $var) {
			$tp = explode(':', $var);
			if ($total < $tp[0]) break;
			$ret = $tp[1];
		}
		return $ret;
	}
	
	/**
	* Писать в журнал ошибки и сообщения
	* @param str $msg запись
	* @param int $level приоритет ошибки/сообщения. Если приоритет больше $this->LOG_LEVEL, то он записан не будет
	**/
	private function log($msg, $level = 0) {
		if ($level > $this->LOG_LEVEL) return;
		$fp = fopen(DIR_LOGS.'maparea_shipping.log', 'a');
		fwrite($fp, date('Y-m-d H:i:s').': '.str_replace("\n", '', $msg)."\n");
		fclose($fp);
	}
}
