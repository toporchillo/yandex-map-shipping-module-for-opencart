<?php 
class ModelShippingMoscow extends Model {    
  	public function getQuote($address) {
		$this->load->language('shipping/moscow');
		
		$quote_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('moscow_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");
	
		if (!$this->config->get('moscow_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}
		
		if (!$status) return array();
		
		$error = false;
		if (!$address['city'] || !$address['address_1']) {
			$error = true;
		}
		else {
			$sess_data = $this->getZamkadCurrent();

			if (isset($sess_data['moscow']) && is_array($sess_data['moscow']) && !isset($sess_data['moscow']['distance'])) {
				$sess_data = $sess_data['moscow'];
				$show_weight = false;
				//++++ Cost for Moscow ++++
				$weight = $this->cart->getWeight();
				$cost = $this->getMoscowCost($weight, $show_weight);
				//---- Cost for outside Moscow ----
				
				if ((string)$cost != '') { 
					$quote_data['moscow'] = array(
						'code'         => 'moscow.moscow',
						'title'        => $this->getMoscowTitle($weight, $show_weight),
						'cost'         => $cost,
						'tax_class_id' => $this->config->get('weight_tax_class_id'),
						'text'         => $this->currency->format($this->tax->calculate($cost, $this->config->get('weight_tax_class_id'), $this->config->get('config_tax')))
					);
				}
				$method_title = $this->language->get('text_title_moscow').$this->getRouteMap($address);
			}
			elseif (isset($sess_data['moscow']) && is_array($sess_data['moscow']) && isset($sess_data['moscow']['distance'])) {
				$sess_data = $sess_data['moscow'];

				$road = $sess_data['distance']; //test
				$show_weight = false;
				//++++ Cost for outside Moscow ++++
				$weight = $this->cart->getWeight();
				$cost = $this->getZamkadCost($weight, $road, $show_weight);
				//---- Cost for outside Moscow ----

				if ((string)$cost == 'toofar') {
					$quote_data['moscow'] = array(
						'code'         => 'moscow.moscow',
						'title'        => sprintf($this->language->get('zamkad_toofar'), (int)$this->config->get('moscow_max_distance')),
						'cost'         => '',
						'distance'	   => $road,
						'tax_class_id' => $this->config->get('moscow_tax_class_id'),
						'text'         => ''
					);
				}
				else if ((string)$cost != '') { 
					$quote_data['moscow'] = array(
						'code'         => 'moscow.moscow',
						'title'        => $this->getZamkadTitle($weight, $road, $show_weight),
						'cost'         => $cost,
						'distance'	   => $road,
						'tax_class_id' => $this->config->get('moscow_tax_class_id'),
						'text'         => $this->currency->format($this->tax->calculate($cost, $this->config->get('moscow_tax_class_id'), $this->config->get('config_tax')))
					);
				}
				$method_title = $this->language->get('text_title_zamkad').$this->getRouteMap($address);
			}
			else {
				$error = true;
			}
		}
		if ($error) {
			$quote_data['moscow'] = array(
				'code'         => 'moscow.moscow',
				'title'        => $this->language->get('zamkad_noaddress'),
				'cost'         => '',
				'distance'	   => 0,
				'tax_class_id' => $this->config->get('moscow_tax_class_id'),
				'text'         => ''
			);
			$method_title = $this->language->get('text_title_moscow');
		}
		
		$method_data = array();
	
		if ($quote_data) {
      		$method_data = array(
        		'code'       => 'moscow',
        		'title'      => $method_title,
        		'quote'      => $quote_data,
				'sort_order' => $this->config->get('moscow_sort_order'),
        		'error'      => false //$error
      		);
		}
		return $method_data;
  	}

  	public function getMoscowTitle($weight, $show_weight) {
		return $this->language->get('moscow_description')
			. ($show_weight ? '  (' . $this->language->get('text_weight') . ' ' . $this->weight->format($weight, $this->config->get('config_weight_class_id')) . ')' : '');
	}

  	public function getZamkadErrTitle() {
		return $this->language->get('zamkad_fault');
	}

  	public function getZamkadTitle($weight, $distance, $show_weight) {
		return $this->language->get('zamkad_description') . ' '
			. number_format(floatval($distance), 2, ',', '') . $this->language->get('km')
			. ($show_weight ? '  (' . $this->language->get('text_weight') . ' ' . $this->weight->format($weight, $this->config->get('config_weight_class_id')) . ')' : '');
	}
	
  	public function getMoscowCost($weight, &$show_weght) {
		$moscost = '';
		$cost_settings = $this->config->get('moscow_moscow_rate');
		
		$rates = explode(',', $cost_settings);
		
		$total = $this->cart->getTotal();
		
		if (strpos($cost_settings, ':') === false) {
			$moscost = floatval($cost_settings);
			$show_weght = false;
		}
		else {
			$rate_data = array();
			foreach ($rates as $i => $rate) {
				$data = explode(':', $rate);
				$rate_data[$i] = $data;
			
				if ($data[0] > $total) {
					break;
				}
				if (isset($data[1])) {
					$moscost = $data[1];
				}
			}
			$show_weght = $show_weght || (count($rates) > 1);
		}
		$show_weght = false;
		return $moscost;
	}
	
  	public function getZamkadCost($weight, $distance, &$show_weght) {
		if ($this->config->get('moscow_max_distance') && $distance > $this->config->get('moscow_max_distance')) {
			$show_weght = false;
			return 'toofar';
		}
		$moscost = $this->getMoscowCost($weight, $show_weght);
		
		$total = $this->cart->getTotal();
		
		$cost_settings = $this->config->get('moscow_zamkad_rate');
		
		if (strpos($cost_settings, ':') === false) {
			$cost = floatval($cost_settings);
			$show_weght = $show_weght || false;
		}
		else {
			$cost = 0;
			$rates = explode(',', $cost_settings);
			
			$rate_data = array();
			foreach ($rates as $i => $rate) {
				$data = explode(':', $rate);
				$rate_data[$i] = $data;
				
				if ($data[0] > $total) {
					break;
				}
				if (isset($data[1])) {
					$cost = $data[1];
				}
			}
			$show_weght = $show_weght || (count($rates) > 1);
		}
		$show_weght = false;
		return $cost * $distance + $moscost;
	}
	
	public function getZamkadCurrent() {
		if (isset($this->session->data['shipping_methods']['moscow']) && isset($this->session->data['shipping_methods']['moscow']['quote'])) {
			return $this->session->data['shipping_methods']['moscow']['quote'];
		}
		return false;
	}
	
  	public function getRouteMap($address) {
		$toaddr = array();
		$toaddr[] = $address['city'];
		$toaddr[] = $address['address_1'];
		$toaddr[] = $address['address_2'];
		$toaddr[] = $address['zone'];
		$toaddr[] = $address['country'];
		$toaddr = array_filter($toaddr, 'strlen');
		$toaddr = implode(', ', $toaddr);
		$toaddr = htmlspecialchars_decode($toaddr);
		
		$zamkad_fault = $this->language->get('zamkad_fault');
		$weight = $this->cart->getWeight();

		$method_title_moscow = $this->language->get('text_title_moscow');
		$method_title_zamkad = $this->language->get('text_title_zamkad');
		$description_moscow = $this->language->get('moscow_description');
		$description_zamkad = $this->language->get('zamkad_description');
		
		return <<<EOD
		
<script type="text/javascript" defer>
$(document).ready(function() {
// Как только будет загружен API и готов DOM, выполняем инициализацию
ymaps.ready(initMap);

function initMap() {
	var myMap = new ymaps.Map("map", {
		center:[55.753559,37.609218],
		zoom:9
	});


	var mkad_km =  [
		[37.830605506897,55.687383285395],[37.830219268799,55.688520279728],[37.829918861389,55.689657241], [37.829747200012,55.690915117077], [37.829618453979,55.692124575141], [37.829575538635,55.694228942985], [37.829661369324,55.695462485266], [37.829918861389,55.696841104105], [37.830305099487,55.698050378846], [37.83073425293,55.699259616176], [37.831721305847,55.701339416874], [37.833652496338,55.705015073026], [37.833952903748,55.705643769059], [37.834510803223,55.706586794142], [37.834939956665,55.707481437928], [37.835283279419,55.70811009428], [37.835927009583,55.709198129448], [37.836484909058,55.710286134317], [37.83721446991,55.711664230324], [37.837901115417,55.712969750123], [37.838373184204,55.7139125984], [37.838802337646,55.715629007354], [37.839059829712,55.717442033494], [37.839231491089,55.719496694721], [37.839531898499,55.721285370603], [37.839789390564,55.723460676814], [37.840647697449,55.731049129674], [37.841076850891,55.734577016296], [37.841591835022,55.737790498523], [37.84197807312,55.741027874718], [37.842192649841,55.743830162627], [37.842364311218,55.747501821492], [37.842364311218,55.749168447351], [37.842578887939,55.753201870331], [37.843008041382,55.760808688711], [37.843222618103,55.765854933608], [37.843351364136,55.772831679656], [37.842149734497,55.784175278519], [37.841033935547,55.79505720214], [37.840304374695,55.801088001038], [37.83974647522,55.808010206659], [37.839274406433,55.812785087524], [37.838931083679,55.815485770861], [37.838416099548,55.818764920104], [37.837944030762,55.820645483954], [37.8369140625,55.822646496952], [37.835712432861,55.82416526944], [37.833952903748,55.826093783977], [37.829360961914,55.829420246856], [37.824683189392,55.831830549447], [37.798676490784,55.845301389845], [37.763228416443,55.863463968387], [37.735333442688,55.877886810141], [37.726020812988,55.882412407382], [37.723488807678,55.883736285551], [37.72087097168,55.885011979978], [37.717480659485,55.886768999884], [37.712631225586,55.889247947481], [37.709283828735,55.890836316102], [37.706279754639,55.892159906959], [37.702202796936,55.893435324525], [37.698984146118,55.894109113107], [37.695508003235,55.894566320125], [37.693104743958,55.894758826731], [37.677097320557,55.895167900098], [37.662334442139,55.895721345435], [37.652807235718,55.896491343206], [37.642593383789,55.897622249727], [37.634825706482,55.89887342704], [37.627873420715,55.900028324129], [37.618217468262,55.902001193785], [37.60959148407,55.904190480455], [37.591309547424,55.9092903174], [37.587919235229,55.910132207116], [37.584271430969,55.910709492358], [37.580065727234,55.911142450649], [37.57577419281,55.911190556828], [37.573843002319,55.911166503746], [37.571740150452,55.910974078555], [37.545218467712,55.90811164109], [37.53830909729,55.907341873959], [37.533974647522,55.906884817495], [37.529940605164,55.906163138427], [37.52676486969,55.905297105819], [37.524018287659,55.90431076735], [37.519211769104,55.902241780781], [37.506508827209,55.896683840261], [37.492604255676,55.890595658369], [37.490329742432,55.889608946058], [37.488570213318,55.888959146201], [37.484407424927,55.887779852004], [37.47350692749,55.884963841327], [37.46844291687,55.883832565839], [37.46621131897,55.883423372968], [37.464151382446,55.883182669264], [37.460460662842,55.883038246326], [37.456512451172,55.882966034655], [37.452864646912,55.882797540234], [37.450761795044,55.882725328115], [37.446298599243,55.882051341863], [37.443380355835,55.881377343909], [37.440590858459,55.880558902089], [37.436814308167,55.879499716472], [37.42814540863,55.876755328395], [37.419991493225,55.874131126795], [37.415614128113,55.872782844161], [37.412266731262,55.871386359223], [37.408962249756,55.869724958283], [37.40617275238,55.868063486266], [37.403554916382,55.86606480981], [37.401323318481,55.864041948198], [37.399349212646,55.861850395902], [37.397804260254,55.859538294628], [37.395958900452,55.856431191852], [37.395186424255,55.855010031027], [37.392525672913,55.849999422292], [37.391710281372,55.848337106496], [37.391581535339,55.846481974493], [37.391924858093,55.844988167491], [37.392268180847,55.844072579986], [37.393298149109,55.842675062743], [37.394242286682,55.841325688079], [37.395272254944,55.839132854357], [37.395701408386,55.837445975026], [37.39574432373,55.835975920504], [37.39574432373,55.834337105692], [37.395272254944,55.832577712921], [37.393641471863,55.827516002194], [37.393126487732,55.825611664305], [37.392525672913,55.822959899305], [37.391667366028,55.820211515748], [37.389006614685,55.811796397656], [37.3881483078,55.809336621212], [37.387933731079,55.808082557709], [37.387118339539,55.806466685553], [37.386302947998,55.804826627319], [37.384243011475,55.801932238352], [37.38055229187,55.798048595134], [37.374887466431,55.79223446741], [37.373514175415,55.790859214839], [37.371668815613,55.788687664549], [37.370767593384,55.787167507314], [37.369866371155,55.784706174591], [37.369394302368,55.772904096077], [37.369351387024,55.769645223914], [37.369136810303,55.766579221382], [37.36900806427,55.763537122309], [37.368965148926,55.758055916224], [37.368836402893,55.752646393725], [37.368793487549,55.751124830341], [37.368879318237,55.749844738986], [37.369050979614,55.748129833798], [37.369523048401,55.746197455939], [37.370252609253,55.744120042994], [37.372097969055,55.740158158242], [37.374801635742,55.734963614623], [37.376003265381,55.732619803504], [37.381238937378,55.722348868778], [37.381925582886,55.720922807787], [37.384114265442,55.716378401695], [37.386045455933,55.712921398316], [37.387118339539,55.711253224322], [37.388362884521,55.709875113816], [37.389822006226,55.708545311983], [37.395143508911,55.704507272692], [37.402439117432,55.699138694127], [37.407460212708,55.695414111792], [37.409477233887,55.693962879743], [37.410507202148,55.693140491001], [37.41183757782,55.691882686521], [37.413339614868,55.689971713813], [37.413811683655,55.68922181293], [37.414755821228,55.687528435449], [37.415614128113,55.68506081126], [37.416472434998,55.683173699459], [37.418661117554,55.679955721884], [37.421536445618,55.676205113177], [37.424411773682,55.672405742901], [37.429990768433,55.664830101048], [37.433981895447,55.660569670981], [37.435612678528,55.658971890141], [37.439732551575,55.655412962049], [37.441878318787,55.653621270371], [37.444281578064,55.651732641731], [37.450547218323,55.646405249588], [37.455439567566,55.642239692715], [37.457714080811,55.640253631738], [37.459344863892,55.638897239544], [37.464451789856,55.634537089387], [37.46509552002,55.633907249811], [37.47522354126,55.625233813813], [37.48110294342,55.620266317065], [37.484793663025,55.617067400604], [37.486596107483,55.615516316846], [37.490673065186,55.612001915699], [37.493805885315,55.609311368166], [37.497925758362,55.605553988419], [37.50114440918,55.60308119375], [37.50479221344,55.59997785831], [37.506680488586,55.598644318454], [37.509770393372,55.596922772705], [37.513160705566,55.595516383498], [37.515306472778,55.594813169991], [37.518353462219,55.59394019776], [37.52423286438,55.592485200883], [37.529940605164,55.591175657566], [37.538952827454,55.589041493342], [37.547879219055,55.586907213062], [37.558436393738,55.584311965914], [37.569122314453,55.581789318261], [37.575645446777,55.580188323167], [37.583198547363,55.578441708603], [37.58873462677,55.577083176889], [37.590537071228,55.576719276484], [37.59379863739,55.576161289312], [37.596802711487,55.575918683721], [37.608733177185,55.57521511903], [37.620363235474,55.574511541733], [37.648215293884,55.572958774813], [37.660102844238,55.572133842414], [37.663836479187,55.571939738154], [37.668600082397,55.571818422505], [37.67156124115,55.571915475055], [37.67692565918,55.57257057349], [37.680015563965,55.573152874036], [37.683362960815,55.574099094008], [37.699284553528,55.579945742458], [37.716794013977,55.586422133176], [37.724561691284,55.589575045278], [37.742285728455,55.596995515603], [37.751512527466,55.600923431832], [37.755331993103,55.602741778394], [37.760739326477,55.605699442075], [37.783613204956,55.618351845394], [37.786831855774,55.620048217416], [37.790608406067,55.621841445167], [37.795414924622,55.624482671778], [37.797174453735,55.625573034529], [37.799062728882,55.626808742312], [37.80734539032,55.632259929206], [37.822022438049,55.641852176529], [37.830905914307,55.647664516776], [37.834210395813,55.649989211358], [37.836313247681,55.652023205913], [37.838544845581,55.655195057981], [37.838973999023,55.655969822489], [37.83974647522,55.658221394811], [37.839918136597,55.659843415048], [37.839789390564,55.661876897705], [37.839403152466,55.663716624318], [37.837471961975,55.669186879392], [37.832622528076,55.68208493969], [37.830562591553,55.687480052157]
	];
	var mkad_km_r =  [];
	for (i=0; i<mkad_km.length; i++) {
		mkad_km_r.push([mkad_km[i][1],mkad_km[i][0]]);
	}

	ymaps.geocode('$toaddr').then(
		function (geocoder) {
		    var point = geocoder.geoObjects.get(0).geometry.getCoordinates();

		    // Создаем полигон МКАД по заданным координатам mkad_km
			var polygon = new ymaps.Polygon(
				[mkad_km_r,[]], {}, {opacity: 0}
			);
		    // Обязательно добавляем на карту, иначе не будет работать
			myMap.geoObjects.add(polygon);

		    if(polygon.geometry.contains(point)) {
		        // Адрес внутри МКАД
				$.getJSON('index.php?route=module/shipping_moscow/moscow_cost&weight=$weight', function(data) {
				  $('label[for="moscow.moscow"]:first').html(data.title);
				  $('label[for="moscow.moscow"]:last').html(data.text);
				  $('.moscow_shipping_title').replaceWith($('$method_title_moscow'));
				  
				  var td1 = $('table.simplecheckout-cart td.price').find("b:contains('$description_moscow')");
				  td1.html(data.title);
				  td1.parents('tr').find('td.total nobr').html(data.text);
				  var td2 = $('table.simplecheckout-cart td.price').find("b:contains('$description_zamkad')");
				  td2.html(data.title);
				  td2.parents('tr').find('td.total nobr').html(data.text);
				  var td3 = $('table.simplecheckout-cart td.price').find(".moscow-error").parent();
				  td3.html(data.title);
				  td3.parents('tr').find('td.total nobr').html(data.text);
				})

				var marker = new ymaps.Placemark(point, { draggable: false });
				myMap.geoObjects.add(marker);
		    }
			else {
				//Построим полный маршрут и затем добавим его на карту.
				ymaps.route([
					'МКАД, 1-й километр',
					{type: 'viaPoint', point: 'МКАД, 17-й километр'},
					{type: 'viaPoint', point: 'МКАД, 34-й километр'},
					{type: 'viaPoint', point: 'МКАД, 51-й километр'},
					{type: 'viaPoint', point: 'МКАД, 68-й километр'},
					{type: 'viaPoint', point: 'МКАД, 84-й километр'},
					{type: 'viaPoint', point: 'МКАД, 98-й километр'},
					'$toaddr'
				], {
				}).then(function (route) {
					  var way = route.getPaths().get(0);
					  segments = way.getSegments();
					  // Маршрут состоит из сегментов. Сегмент - участок маршрута, который нужно проехать
					  // до следующего изменения направления движения.
					  var new_route = [];
					  var road = 0;
					  for (var i = segments.length - 1; i > 0; i--) {
						  if (segments[i].getStreet() == 'МКАД') break;
						  new_route.push(segments[i].getCoordinates()[1]);
						  road = road + segments[i].getLength();
					  }
					  road = road/1000;
					  console.log('Distance: ', road);
					  $.getJSON('index.php?route=module/shipping_moscow/zamkad_cost&weight=$weight&zamkad_distance=' + road, function(data) {
						$('label[for="moscow.moscow"]:first').html(data.title);
						$('label[for="moscow.moscow"]:last').html(data.text);
						$('.moscow_shipping_title').replaceWith($('$method_title_zamkad'));
						
						var td1 = $('table.simplecheckout-cart td.price').find("b:contains('$description_moscow')");
						td1.html(data.title);
						td1.parents('tr').find('td.total nobr').html(data.text);
						var td2 = $('table.simplecheckout-cart td.price').find("b:contains('$description_zamkad')");
						td2.html(data.title);
						td2.parents('tr').find('td.total nobr').html(data.text);
						var td3 = $('table.simplecheckout-cart td.price').find(".moscow-error").parent();
						td3.html(data.title);
						td3.parents('tr').find('td.total nobr').html(data.text);
					  })
					  ymaps.route([new_route[new_route.length - 1], '$toaddr'], {
						  mapStateAutoApply:true
					  }).then(function (_route) {
						myMap.geoObjects.add(_route);
					  })
				}, function (error) {
					addressFault();
				});
			}
		},
		function(err) {
			addressFault();
		}
	)
}

function addressFault() {
	$.getJSON('index.php?route=module/shipping_moscow/zamkad_fault', function(data) {
		$('label[for="moscow.moscow"]:first').html('$zamkad_fault');
		$('label[for="moscow.moscow"]:last').html('');

		var is_checked = $('input[value="moscow.moscow"]').is(':checked');
		$('input[value="moscow.moscow"]').removeAttr('checked');
		if (is_checked) $('input[value="moscow.moscow"]').trigger('change');
	})
}
})
</script>
<div id="map" style="width: 100%; height: 350px;"> </div>
EOD;
	}
}
?>
