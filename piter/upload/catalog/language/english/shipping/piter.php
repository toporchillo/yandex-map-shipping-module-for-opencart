<?php
// Text
$_['text_title']  = 'Доставка по Санкт-Петерургу и за КАД';
$_['text_weight'] = 'Вес:'; 
$_['km']  = ' км.';
$_['text_title_piter'] = '<span class="piter_shipping_title">Доставка по Санкт-Петербургу</span>';
$_['text_title_piterkad'] = '<span class="piter_shipping_title">Доставка в пределах КАД</span>';
$_['piter_description'] = 'Стоимость доставки по Санкт-Петербургу';
$_['piterkad_description'] = 'Стоимость доставки в пределах КАД';
$_['text_title_zamkad'] = '<span class="piter_shipping_title">Доставка за КАД</span>';
$_['zamkad_description'] = 'Расстояние от КАД';
$_['zamkad_noaddress'] = '<em class="error piter-error" style="display: inline;">Для рассчета стоимости доставки укажите населенный пункт и адрес доставки.</em>';
$_['zamkad_fault'] = '<em class="error piter-error" style="display: inline;">Не удалось рассчитать расстояние. Стоимость доставки Вы узнаете после оформления заказа.</em>';
$_['zamkad_toofar'] = '<em class="error piter-error" style="display: inline;">Извините, мы не доставляем заказы дальше, чем %d км за КАД. Выберите другой способ доставки.</em>';
?>
