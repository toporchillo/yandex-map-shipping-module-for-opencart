<?php  
class ControllerModuleShippingPiter extends Controller {
	public function piter_cost() {
		$this->load->language('shipping/piter');
		$weight = floatval($this->request->get['weight']);
		$this->load->model('shipping/piter');
		$show_weight = false;
		$cost = $this->model_shipping_piter->getPiterCost($weight, $show_weight);
			
		$text = $this->currency->format($this->tax->calculate($cost, $this->config->get('piter_tax_class_id'), $this->config->get('config_tax')));
		
		$ret = array(
				'code'         => 'piter.piter',
				'title'        => $this->model_shipping_piter->getPiterTitle($weight, $show_weight),
				'cost'         => $cost,
				'tax_class_id' => $this->config->get('piter_tax_class_id'),
				'text'         => $text);
		if (isset($this->session->data['shipping_methods']['piter']) && isset($this->session->data['shipping_methods']['piter']['quote'])) {
			$this->session->data['shipping_methods']['piter']['quote']['piter'] = $ret;
		}
		
		$this->response->setOutput(json_encode($ret));
	}

	public function piterkad_cost() {
		$this->load->language('shipping/piter');
		$weight = floatval($this->request->get['weight']);
		$this->load->model('shipping/piter');
		$show_weight = false;
		$cost = $this->model_shipping_piter->getPiterkadCost($weight, $show_weight);
			
		$text = $this->currency->format($this->tax->calculate($cost, $this->config->get('piter_tax_class_id'), $this->config->get('config_tax')));
		
		$ret = array(
				'code'         => 'piter.piter',
				'title'        => $this->model_shipping_piter->getPiterkadTitle($weight, $show_weight),
				'cost'         => $cost,
				'tax_class_id' => $this->config->get('piter_tax_class_id'),
				'text'         => $text);
		if (isset($this->session->data['shipping_methods']['piter']) && isset($this->session->data['shipping_methods']['piter']['quote'])) {
			$this->session->data['shipping_methods']['piter']['quote']['piter'] = $ret;
		}
		
		$this->response->setOutput(json_encode($ret));
	}

	public function zamkad_cost() {
		$this->load->language('shipping/piter');
		$weight = floatval($this->request->get['weight']);
		$distance = floatval($this->request->get['zamkad_distance']);
		if ($distance > 0) {
			$this->load->model('shipping/piter');
			$show_weight = false;
			$cost = $this->model_shipping_piter->getZamkadCost($weight, $distance, $show_weight);
			
			if ($cost == 'toofar') {
				$ret = array(
						'code'         => 'moscow.moscow',
						'title'        => sprintf($this->language->get('zamkad_toofar'), (int)$this->config->get('moscow_max_distance')),
						'cost'         => 0,
						'distance'	   => 'error',
						'tax_class_id' => $this->config->get('moscow_tax_class_id'),
						'text'         => '');
			}
			else {
				$text = $this->currency->format($this->tax->calculate($cost, $this->config->get('piter_tax_class_id'), $this->config->get('config_tax')));
				$ret = array(
						'code'         => 'piter.piter',
						'title'        => $this->model_shipping_piter->getZamkadTitle($weight, $distance, $show_weight),
						'cost'         => $cost,
						'distance'	   => $distance,
						'tax_class_id' => $this->config->get('piter_tax_class_id'),
						'text'         => $text);
			}
			if (isset($this->session->data['shipping_methods']['piter']) && isset($this->session->data['shipping_methods']['piter']['quote'])) {
				$this->session->data['shipping_methods']['piter']['quote']['piter'] = $ret;
			}
			
			$this->response->setOutput(json_encode($ret));
		}
	}

	public function zamkad_fault() {
		$this->load->language('shipping/piter');
		$this->load->model('shipping/piter');
		
		$ret = array(
				'code'         => 'piter.piter',
				'title'        => $this->model_shipping_piter->getZamkadErrTitle(),
				'cost'         => 0,
				'distance'	   => 'error',
				'tax_class_id' => $this->config->get('piter_tax_class_id'),
				'text'         => '');
						
		if (isset($this->session->data['shipping_methods']['piter']) && isset($this->session->data['shipping_methods']['piter']['quote'])) {
			$this->session->data['shipping_methods']['piter']['quote']['piter'] = $ret;
		}
		$this->response->setOutput(json_encode($ret));
	}
}
?>
