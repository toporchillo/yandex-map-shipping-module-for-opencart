<?php
// Heading
$_['heading_title']      = 'Доставка по Санкт-Петербургу и за КАД';

// Text
$_['text_shipping']      = 'Доставка';
$_['text_success']       = 'Настройки модуля обновлены!';

// Entry
$_['entry_piter_rate']  = 'Расценки по Санкт-Петербургу:<br /><span class="help">Например: 5:300.00,10:350.00 Вес:Цена,Вес:Цена, и т.д.</span>';
$_['entry_piterkad_rate']  = 'Расценки в пределах КАД, но не СПб.:<br /><span class="help">Например: 5:400.00,10:450.00 Вес:Цена,Вес:Цена, и т.д.</span>';
$_['entry_zamkad_rate']  = 'Расценки за километр за КАД:<br /><span class="help">Например: 5:15.00,10:17.00 Вес:Цена,Вес:Цена, и т.д.</span>';
$_['entry_max_distance']  = 'Максимальное расстояние доставки (км):<br /><span class="help">Если расстояние от КАД больше, то доставка будет недоступна.</span>';

$_['entry_tax_class']    = 'Налоговый класс:';
$_['entry_geo_zone']     = 'Географическая зона:';
$_['entry_status']       = 'Статус:';
$_['entry_sort_order']   = 'Порядок сортировки:';

// Error
$_['error_permission']   = 'У Вас нет прав для управления этим модулем!';
?>
